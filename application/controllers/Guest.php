<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Guest extends CI_Controller {

	public function index()
	{
		$this->load->view("guest/dashboard");
	}

	public function produk()
	{
		$this->load->view("guest/dashboard");
	}

	public function keranjang()
	{
		
	}

	public function galeri()
	{
		# code...
	}

	public function logout()
	{
		// $this->CI->session->unset_userdata('id_user');
		// $this->CI->session->unset_userdata('nama');
		// $this->CI->session->unset_userdata('username');
		// $this->CI->session->unset_userdata('type_user');
		$this->session->sess_destroy();
		redirect('login','refresh');
	}

}

/* End of file Guest.php */
/* Location: ./application/controllers/Guest.php */
