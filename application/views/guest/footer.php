	<!--== Footer Area Start ==-->
	<section id="footer-area">
		<!-- Footer Widget Start -->
		<div class="footer-widget-area">
			<div class="container">
				<div class="row">
					<!-- Single Footer Widget Start -->
					<div class="col-lg-4 col-md-6">
						<div class="single-footer-widget">
							<div class="widget-body">
								<img src="assets/cardoor/img/logo.png">
							</div>
						</div>
					</div>
					<!-- Single Footer Widget End -->

					<!-- Single Footer Widget Start -->
					<div class="col-lg-4 col-md-6">
						<div class="single-footer-widget">
							<h2>Ciliwung Camp</h2>
							<div class="widget-body">
								<ul class="get-touch">
									<li><i class="fa fa-map-marker"></i> 800/8, Kazipara, Dhaka</li>
									<li><i class="fa fa-mobile"></i>0851-0396-5552 (OFFICE)</li>
									<li><i class="fa fa-mobile"></i>0812- 735-9468 (ANDRE)</li>
									<li><i class="fa fa-envelope"></i> kazukamdu83@gmail.com</li>
								</ul>
								<a href="https://g.page/CiliwungCamp?share" class="map-show" target="_blank">Tunjukan Lokasi</a>
							</div>
						</div>
					</div>
					<!-- Single Footer Widget End -->
				</div>
			</div>
		</div>
		<!-- Footer Widget End -->
	</section>
	<!--== Scroll Top Area Start ==-->
	<div class="scroll-top">
		<img src="<?php echo base_url() ?>assets/img/scroll-top.png" alt="JSOFT">
	</div>
	<!--== Scroll Top Area End ==-->

	<!--=======================Javascript============================-->
	<!--=== Jquery Min Js ===-->
	<script src="<?php echo base_url('assets/cardoor/js/jquery-3.2.1.min.js') ?>"></script>
	<!--=== Jquery Migrate Min Js ===-->
	<script src="<?php echo base_url('assets/cardoor/js/jquery-migrate.min.js') ?>"></script>
	<!--=== Popper Min Js ===-->
	<script src="<?php echo base_url('assets/cardoor/js/popper.min.js') ?>"></script>
	<!--=== Bootstrap Min Js ===-->
	<script src="<?php echo base_url('assets/cardoor/js/bootstrap.min.js') ?>"></script>
	<!--=== Gijgo Min Js ===-->
	<script src="<?php echo base_url('assets/cardoor/js/plugins/gijgo.js') ?>"></script>
	<!--=== Vegas Min Js ===-->
	<script src="<?php echo base_url('assets/cardoor/js/plugins/vegas.min.js') ?>"></script>
	<!--=== Isotope Min Js ===-->
	<script src="<?php echo base_url('assets/cardoor/js/plugins/isotope.min.js') ?>"></script>
	<!--=== Owl Caousel Min Js ===-->
	<script src="<?php echo base_url('assets/cardoor/js/plugins/owl.carousel.min.js') ?>"></script>
	<!--=== Waypoint Min Js ===-->
	<script src="<?php echo base_url('assets/cardoor/js/plugins/waypoints.min.js') ?>"></script>

	<!--=== CounTotop Min Js ===-->
	<script src="<?php echo base_url('assets/cardoor/js/plugins/counterup.min.js') ?>"></script>
	<!--=== YtPlayer Min Js ===-->
	<script src="<?php echo base_url('assets/cardoor/js/plugins/mb.YTPlayer.js') ?>"></script>
	<!--=== Magnific Popup Min Js ===-->
	<script src="<?php echo base_url('assets/cardoor/js/plugins/magnific-popup.min.js') ?>"></script>
	<!--=== Slicknav Min Js ===-->
	<script src="<?php echo base_url('assets/cardoor/js/plugins/slicknav.min.js') ?>"></script>

	<!--=== Mian Js ===-->
	<script src="<?php echo base_url('assets/cardoor/js/main.js') ?>"></script>


</body>

</html>
