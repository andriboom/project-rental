<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->view('guest/header');
?>
<!--== Mobile App Area Start ==-->
<div id="mobileapp-video-bg"></div>
<section id="mobile-app-area">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="mobile-app-content">
					<center><h2>Selamat Datang Ciliwung Camp</h2>
						<p>Easy &amp; Fast - Book a tent</p>
                       <!--  <div class="app-btns">
                            <a href="#"><i class="fa fa-android"></i> Produk </a>
                            <a href="#"><i class="fa fa-apple"></i> Apple Store</a>
                        </div></center> -->

                        <div class="book-ur-car">
                        	<form action="index2.html">
                        		<div class="pick-location bookinput-item">
                        			<select class="custom-select">
                        				<option selected>Pick Location</option>
                        				<option value="1">Dhaka</option>
                        				<option value="2">Comilla</option>
                        				<option value="3">Barishal</option>
                        				<option value="3">Rangpur</option>
                        			</select>
                        		</div>

                        		<div class="pick-date bookinput-item">
                        			<input id="startDate2" placeholder="Pick Date" />
                        		</div>

                        		<div class="retern-date bookinput-item">
                        			<input id="endDate2" placeholder="Return Date" />
                        		</div>

                        		<div class="car-choose bookinput-item">
                        			<select class="custom-select">
                        				<option selected>Choose Car</option>
                        				<option value="1">BMW</option>
                        				<option value="2">Audi</option>
                        				<option value="3">Lexus</option>
                        			</select>
                        		</div>

                        		<div class="bookcar-btn bookinput-item">
                        			<button type="submit">Book Car</button>
                        		</div>
                        	</form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--== Mobile App Area End ==-->
    <!--== SlideshowBg Area End ==-->

    <!--== About Us Area Start ==-->
    <section id="about-area" class="section-padding">
    	<div class="container">
    		<div class="row">
    			<!-- Section Title Start -->
    			<div class="col-lg-12">
    				<div class="section-title  text-center">
    					<h2>About us</h2>
    					<span class="title-line"><i class="fa fa-car"></i></span>
    					<p>Layanan Penyewaan Peralatan Outdoor</p>
    				</div>
    			</div>
    			<!-- Section Title End -->
    		</div>

    		<div class="row">
    			<!-- About Content Start -->
    			<div class="col-lg-6">
    				<div class="display-table">
    					<div class="display-table-cell">
    						<div class="about-content">
    							<p>Ciliwung Camp adalah perusahaan yang berfokus pada penyedia jasa, sarana dan prasarana yang mendukung aktivitas outdoor.
    							Ciliwung Camp memiliki tujuan memberikan sarana bagi para pecinta kegiatan outdoor serta dedikasi tinggi untuk memberikan pelayanan demi kepuasan konsumen. Semua kemajuan yang dicapai tidak lepas dari nilai-nilai yang telah kami yakini sejak awal serta terjalinnya hubungan kekeluargaan yang dekat dengan konsumen, karyawan, pemilik, dan pihak – pihak lain yang terkait.</p>
    						</div>
    					</div>
    				</div>
    			</div>
    			<!-- About Content End -->

    			<!-- About Video Start -->
    			<div class="col-lg-6">
    				<div class="about-image">
    					<img src="<?php echo base_url() ?>assets/img/article/ciliwung-camp.jpg" alt="JSOFT">
    				</div>
    			</div>
    			<!-- About Video End -->
    		</div>


    		<!-- About Fretutes Start -->
    		<div class="about-feature-area">
    			<div class="row">
    				<!-- Single Fretutes Start -->
    				<div class="col-lg-4">
    					<div class="about-feature-item active">
    						<i class="fa fa-car"></i>
    						<h3>Penyewaan Peralatan Outdoor</h3>
    						<p>Melayani jasa penyewaan alat outdoor </p>
    					</div>
    				</div>
    				<!-- Single Fretutes End -->

    				<!-- Single Fretutes Start -->
    				<div class="col-lg-4">
    					<div class="about-feature-item">
    						<i class="fa fa-car"></i>
    						<h3>Outbond & inbound Trainning</h3>
    						<p>Melayani paket outbound dan inbound sesuai paket yang disediakan</p>
    					</div>
    				</div>
    				<!-- Single Fretutes End -->

    				<!-- Single Fretutes Start -->
    				<div class="col-lg-4">
    					<div class="about-feature-item">
    						<i class="fa fa-car"></i>
    						<h3>Adventure City Tour</h3>
    						<p>Melayani jasa paket tour</p>
    					</div>
    				</div>
    				<!-- Single Fretutes End -->
    			</div>
    		</div>
    		<!-- About Fretutes End -->
    	</div>
    </section>
    <!--== About Us Area End ==-->

    <!--== Partner Area Start ==-->
    <div id="partner-area">
    	<div class="container-fluid">
    		<div class="row">
    			<div class="col-lg-12 text-center">
    				<div class="partner-content-wrap">
    					<!-- Single Partner Start -->
    					<div class="single-partner">
    						<div class="display-table">
    							<div class="display-table-cell">
    								<img src="<?php echo base_url() ?>assets/img/partner/logo1.png" alt="JSOFT">
    								<p>Tenda</p>
    							</div>
    						</div>
    					</div>
    					<!-- Single Partner End -->

    					<!-- Single Partner Start -->
    					<div class="single-partner">
    						<div class="display-table">
    							<div class="display-table-cell">
    								<img src="<?php echo base_url() ?>assets/img/partner/logo2.png" alt="JSOFT">
    							</div>
    						</div>
    					</div>
    					<!-- Single Partner End -->

    					<!-- Single Partner Start -->
    					<div class="single-partner">
    						<div class="display-table">
    							<div class="display-table-cell">
    								<img src="<?php echo base_url() ?>assets/img/partner/logo3.png" alt="JSOFT">
    							</div>
    						</div>
    					</div>
    					<!-- Single Partner End -->

    					<!-- Single Partner Start -->
    					<div class="single-partner">
    						<div class="display-table">
    							<div class="display-table-cell">
    								<img src="<?php echo base_url() ?>assets/img/partner/logo4.png" alt="JSOFT">
    							</div>
    						</div>
    					</div>
    					<!-- Single Partner End -->

    					<!-- Single Partner Start -->
    					<div class="single-partner">
    						<div class="display-table">
    							<div class="display-table-cell">
    								<img src="<?php echo base_url() ?>assets/img/partner/logo5.png" alt="JSOFT">
    							</div>
    						</div>
    					</div>
    					<!-- Single Partner End -->

    					<!-- Single Partner Start -->
    					<div class="single-partner">
    						<div class="display-table">
    							<div class="display-table-cell">
    								<img src="<?php echo base_url() ?>assets/img/partner/partner-logo-1.png" alt="JSOFT">
    							</div>
    						</div>
    					</div>
    					<!-- Single Partner End -->

    					<!-- Single Partner Start -->
    					<div class="single-partner">
    						<div class="display-table">
    							<div class="display-table-cell">
    								<img src="<?php echo base_url() ?>assets/img/partner/partner-logo-4.png" alt="JSOFT">
    							</div>
    						</div>
    					</div>
    					<!-- Single Partner End -->
    				</div>
    			</div>
    		</div>
    	</div>
    </div>
    <!--== Partner Area End ==-->

    <!--== Services Area Start ==-->
    <section id="service-area" class="section-padding">
    	<div class="container">
    		<div class="row">
    			<!-- Section Title Start -->
    			<div class="col-lg-12">
    				<div class="section-title  text-center">
    					
    					<p><h2>Barang Yang Dapat Di Sewa</h2></p>
    					<br>
    				</div>
    			</div>
    			<!-- Section Title End -->
    		</div>

    		<!-- Service Content Start -->
    		<div class="row">
    			<div class="col-lg-11 m-auto text-center">
    				<div class="service-container-wrap">
    					<!-- Single Service Start -->
    					<div class="service-item">
    						<i class="fa fa-taxi"></i>
    						<h3>Peralatan Berkemah</h3>
    						
    					</div>
    					<!-- Single Service End -->

    					<!-- Single Service Start -->
    					<div class="service-item">
    						<img src="<?php echo base_url() ?>assets/img/partner/1.png" alt="JSOFT">
    						<h3>Tenda</h3>
    						<p></p>
    					</div>
    					<!-- Single Service End -->

    					<!-- Single Service Start -->
    					<div class="service-item">
    						<i class="fa fa-map-marker"></i>
    						<h3>TAXI SERVICE</h3>
    						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit admollitia.</p>
    					</div>
    					<!-- Single Service End -->

    					<!-- Single Service Start -->
    					<div class="service-item">
    						<i class="fa fa-life-ring"></i>
    						<h3>life insurance</h3>
    						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit admollitia.</p>
    					</div>
    					<!-- Single Service End -->

    					<!-- Single Service Start -->
    					<div class="service-item">
    						<i class="fa fa-bath"></i>
    						<h3>car wash</h3>
    						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit admollitia.</p>
    					</div>
    					<!-- Single Service End -->

    					<!-- Single Service Start -->
    					<div class="service-item">
    						<i class="fa fa-phone"></i>
    						<h3>call driver</h3>
    						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit admollitia.</p>
    					</div>
    					<!-- Single Service End -->
    				</div>
    			</div>
    		</div>
    		<!-- Service Content End -->
    	</div>
    </section>
    <!--== Services Area End ==-->

    <!--== Fun Fact Area Start ==-->
    <section id="funfact-area" class="overlay section-padding">
    	<div class="container">
    		<div class="row">
    			<div class="col-lg-11 col-md-12 m-auto">
    				<div class="funfact-content-wrap">
    					<div class="row">
    						<!-- Single FunFact Start -->
    						<div class="col-lg-4 col-md-6">
    							<div class="single-funfact">
    								<div class="funfact-icon">
    									<i class="fa fa-smile-o"></i>
    								</div>
    								<div class="funfact-content">
    									<p><span class="counter">550</span>+</p>
    									<h4>HAPPY CLIENTS</h4>
    								</div>
    							</div>
    						</div>
    						<!-- Single FunFact End -->

    						<!-- Single FunFact Start -->
    						<div class="col-lg-4 col-md-6">
    							<div class="single-funfact">
    								<div class="funfact-icon">
    									<i class="fa fa-car"></i>
    								</div>
    								<div class="funfact-content">
    									<p><span class="counter">250</span>+</p>
    									<h4>CARS IN STOCK</h4>
    								</div>
    							</div>
    						</div>
    						<!-- Single FunFact End -->

    						<!-- Single FunFact Start -->
    						<div class="col-lg-4 col-md-6">
    							<div class="single-funfact">
    								<div class="funfact-icon">
    									<i class="fa fa-bank"></i>
    								</div>
    								<div class="funfact-content">
    									<p><span class="counter">50</span>+</p>
    									<h4>office in cities</h4>
    								</div>
    							</div>
    						</div>
    						<!-- Single FunFact End -->
    					</div>
    				</div>
    			</div>
    		</div>
    	</div>
    </section>
    <!--== Fun Fact Area End ==-->
</div>
<!-- Choose Cars Content-wrap -->
</div>
</div>
</div>
</div>
<!-- Choose Area Content End -->
</div>
</div>
</section>
<!--== Choose Car Area End ==-->

<!-- Single Team  End -->
</div>
</div>
<!-- Team Tab Content End -->
</div>
</div>
</div>
</div>
</div>
</section>
<!--== Team Area End ==-->


<!--== Articles Area Start ==-->
<section id="tips-article-area" class="section-padding">
	<div class="container">
		<div class="row">
			<!-- Section Title Start -->
			<div class="col-lg-12">
				<div class="section-title  text-center">
					<h2>Tips and articles</h2>
					<span class="title-line"><i class="fa fa-car"></i></span>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
				</div>
			</div>
			<!-- Section Title End -->
		</div>

		<!-- Articles Content Wrap Start -->
		<div class="row">
			<!-- Single Articles Start -->
			<div class="col-lg-12">
				<article class="single-article">
					<div class="row">
						<!-- Articles Thumbnail Start -->
						<div class="col-lg-5">
							<div class="article-thumb">
								<img src="<?php echo base_url() ?>assets/img/article/PANDERMAN.jpg" alt="JSOFT">
							</div>
						</div>
						<!-- Articles Thumbnail End -->

						<!-- Articles Content Start -->
						<div class="col-lg-7">
							<div class="display-table">
								<div class="display-table-cell">
									<div class="article-body">
										<h3><a href="https://www.ciliwungcamp.com/5-gunung-terbaik-untuk-mendaki-di-malang-raya/">5 Gunung Terbaik Untuk Mendaki Di Malang RayaS</a></h3>
                                  <!--   <div class="article-meta">
                                        <a href="#" class="author">By :: <span>Admin</span></a>
                                        <a href="#" class="commnet">Comments :: <span>10</span></a>
                                    </div>
                                -->
                                <div class="article-date">25 <span class="month">jan</span></div>

                                <p>Malang merupakan salah satu kota pilihan favorit sewa tenda malang untuk wisata di Jawa Timur. Malang mempunyai banyak destinasi wisata alam yang menarik seperti pantai, coban atau air terjun, dan dari semua pilihan itu gunung trip bromo lah yang paling terkenal.  </p>

                                <a href="https://www.ciliwungcamp.com/5-gunung-terbaik-untuk-mendaki-di-malang-raya/" class="readmore-btn">Read More <i class="fa fa-long-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Articles Content End -->
            </div>
        </article>
    </div>
    <!-- Single Articles End -->

    <!-- Single Articles Start -->
    <div class="col-lg-12">
    	<article class="single-article middle">
    		<div class="row">

    			<!-- Articles Content Start -->
    			<div class="col-lg-7">
    				<div class="display-table">
    					<div class="display-table-cell">
    						<div class="article-body">
    							<h3><a href="https://www.ciliwungcamp.com/explore-trip-bromo-siang-hari/">Explore Trip Bromo Siang Hari</a></h3>
    							<div class="article-meta">
    								<a href="#" class="author">By :: <span>Admin</span></a>
    								<a href="#" class="commnet">Comments :: <span>10</span></a>
    							</div>

    							<div class="article-date">14<span class="month">feb</span></div>

    							<p>Gunung Bromo merupakan gunung berapi yang masih aktif dan menjadi salah satu destinasi favorit para pelancong trip bromo Indonesia maupun mancanegara.Konon katanya, Gunung Bromo terbentuk dari letusan dahsyat Gunung Tengger yang membentuk kaldera dengan diameter kurang lebih dari 8 kilometer. .</p>

    							<a href="https://www.ciliwungcamp.com/explore-trip-bromo-siang-hari/" class="readmore-btn">Read More <i class="fa fa-long-arrow-right"></i></a>
    						</div>
    					</div>
    				</div>
    			</div>
    			<!-- Articles Content End -->

    			<!-- Articles Thumbnail Start -->
    			<div class="col-lg-5 d-none d-xl-block">
    				<div class="article-thumb">
    					<img src="<?php echo base_url() ?>assets/img/article/EXPLORE.jpg" alt="JSOFT">
    				</div>
    			</div>
    			<!-- Articles Thumbnail End -->
    		</div>
    	</article>
    </div>
    <!-- Single Articles End -->

    <!-- Single Articles Start -->
    <div class="col-lg-12">
    	<article class="single-article">
    		<div class="row">
    			<!-- Articles Thumbnail Start -->
    			<div class="col-lg-5">
    				<div class="article-thumb">
    					<img src="<?php echo base_url() ?>assets/img/article/ARJUNO.jpg" alt="JSOFT">
    				</div>
    			</div>
    			<!-- Articles Thumbnail End -->

    			<!-- Articles Content Start -->
    			<div class="col-lg-7">
    				<div class="display-table">
    					<div class="display-table-cell">
    						<div class="article-body">
    							<h3><a href="https://www.ciliwungcamp.com/alur-pendakian-gunung-arjuno-2019/">Alur Pendakian Gunung Arjuno 2019</a></h3>
    							<div class="article-meta">
    								<a href="#" class="author">By :: <span>Admin</span></a>
    								<a href="#" class="commnet">Comments :: <span>10</span></a>
    							</div>

    							<div class="article-date">17 <span class="month">feb</span></div>

    							<p>Beberapa gunung dengan ketinggian lebih dari 3.000 meter di atas permukaan laut (mdpl), dan salah satunya adalah gunung tertinggi di pulau jawa, Gunung Arjuno adalah sebuah gunung berapi kerucut di Jawa Timur, Indonesia dengan ketinggian 3.339 m dpl.</p>

    							<a href="https://www.ciliwungcamp.com/alur-pendakian-gunung-arjuno-2019/" class="readmore-btn">Read More <i class="fa fa-long-arrow-right"></i></a>
    						</div>
    					</div>
    				</div>
    			</div>
    			<!-- Articles Content End -->
    		</div>
    	</article>
    </div>
    <!-- Single Articles End -->
</div>
<!-- Articles Content Wrap End -->
</div>
</section>
<!--== Articles Area End ==-->
<?php $this->load->view('guest/footer'); ?>
