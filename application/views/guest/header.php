<!DOCTYPE html>
<html class="no-js" lang="zxx">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!--=== Favicon ===-->
	<link rel="shortcut icon"  href="<?php echo base_url('assets/cardoor/img/favicon.ico') ?>"  type="image/x-icon" />

	<title>Ciliwung Camp</title>

	<!--=== Bootstrap CSS ===-->
	<link rel="stylesheet" href="<?php echo base_url('assets/cardoor/css/bootstrap.min.css') ?>">
	<!--=== Vegas Min CSS ===-->
	<link rel="stylesheet" href="<?php echo base_url('assets/cardoor/css/plugins/vegas.min.css') ?>">
	<!--=== Slicknav CSS ===-->
	<link rel="stylesheet" href="<?php echo base_url('assets/cardoor/css/plugins/slicknav.min.css') ?>">
	<!--=== Magnific Popup CSS ===-->
	<link rel="stylesheet" href="<?php echo base_url('assets/cardoor/css/plugins/magnific-popup.css') ?>">
	<!--=== Owl Carousel CSS ===-->
	<link rel="stylesheet" href="<?php echo base_url('assets/cardoor/css/plugins/owl.carousel.min.css') ?>">
	<!--=== Gijgo CSS ===-->
	<link rel="stylesheet" href="<?php echo base_url('assets/cardoor/css/plugins/gijgo.css') ?>">
	<!--=== FontAwesome CSS ===-->
	<link rel="stylesheet" href="<?php echo base_url('assets/cardoor/css/font-awesome.css') ?>">
	<!--=== Theme Reset CSS ===-->
	<link rel="stylesheet" href="<?php echo base_url('assets/cardoor/css/reset.css') ?>">
	<!--=== Main Style CSS ===-->
	<link rel="stylesheet" href="<?php echo base_url('assets/cardoor/style.css') ?>">
	<!--=== Responsive CSS ===-->
	<link rel="stylesheet" href="<?php echo base_url('assets/cardoor/css/responsive.css') ?>">
</head>
<body class="loader-active">
	<!--== Header Area Start ==-->
	<header id="header-area" class="fixed-top">
		<!--== Header Bottom Start ==-->
		<div id="header-bottom" class="d-none d-xl-block">
			<div class="container">
				<div class="row">
					<!--== Logo Start ==-->
					<div class="col-lg-4">
						<a href="index2.html" >
							<img src="<?php echo base_url() ?>assets/cardoor/img/logo10.png" alt="JSOFT">
						</a>
					</div>
					<!--== Logo End ==-->

					<!--== Main Menu Start ==-->
					<div class="col-lg-8 d-none d-xl-block">
						<nav class="mainmenu alignright">
							<ul>
								<li class="active"><a href='<?php echo base_url(); ?>'>Home</a></li>
								<li><a href='<?php echo base_url("produk"); ?>'>Produk</a>
									<li><a href='<?php echo base_url("keranjang"); ?>'>Keranjang</a></li>
									<li><a href="#">Galery</a></li>
									<li><a href='<?php echo base_url("login"); ?>'>Login</a></li>
								</ul>
							</nav>
						</div>
						<!--== Main Menu End ==-->
					</div>
				</div>
			</div>
			<!--== Header Bottom End ==-->
		</header>
		<!--== Header Area End ==-->
