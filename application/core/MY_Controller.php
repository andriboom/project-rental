<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

	public function cekHakAkses()
	{
		if (!empty($this->session->userdata('type_user'))) {
			if ($this->session->userdata('type_user')=="3") {
				redirect('admin','refresh');
			}else if ($this->session->userdata('type_user')=="2") {
				redirect('maintenance','refresh');
			}else if ($this->session->userdata('type_user')=="1") {
				redirect('customer','refresh');
			}
		}
	}

	public function cekAksesAdmin()
	{
		if (!empty($this->session->userdata('id_user'))) {
			if ($this->session->userdata('type_user')!="3" ) {
				redirect('login','refresh');
			}
		}else{
			redirect('login','refresh');
		}
	}

	public function destroySession()
	{
		
		$this->CI->session->sess_destroy();
		return true;
	}

}

/* End of file MY_Controller.php */
/* Location: ./application/core/MY_Controller.php */
